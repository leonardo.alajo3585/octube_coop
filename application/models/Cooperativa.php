<?php

class Cooperativa extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function obtenerTodos()
    {
        $cooperativas = $this->db->get('cooperativa');
        if ($cooperativas->num_rows() > 0) {
            return $cooperativas->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_coo', $id);
        $cooperativa = $this->db->get('cooperativa');
        if ($cooperativa->num_rows() > 0) {
            return $cooperativa->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_coo', $id);
        return $this->db->update('cooperativa', $datos);
    }

}
