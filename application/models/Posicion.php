<?php

class Posicion extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert('posicion', $datos);
    }

    function obtenerTodos()
    {
        $posiciones = $this->db->get('posicion');
        if ($posiciones->num_rows() > 0) {
            return $posiciones->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id)
    {
        $this->db->where('id_pos', $id);
        $posicion = $this->db->get('posicion');
        if ($posicion->num_rows() > 0) {
            return $posicion->row();
        } else {
            return false;
        }
    }

    function obtenerNombrePorId($id)
    {
        $this->db->select('nombre_pos');
        $this->db->where('id_pos', $id);
        $query = $this->db->get('posicion');

        if ($query->num_rows() > 0) {
            return $query->row()->nombre_pos;
        }

        return null;
    }

    function eliminar($id)
    {
        try {
            $this->db->where('id_pos', $id);
            return $this->db->delete('posicion');
        } catch (\Throwable $th) {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where('id_pos', $id);
        return $this->db->update('posicion', $datos);
    }
}
?>
