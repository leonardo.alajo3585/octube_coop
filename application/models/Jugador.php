<?php

class Jugador extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insertar($datos) {
        return $this->db->insert('jugador', $datos);
    }

    function obtenerTodos() {
        $jugadores = $this->db->get('jugador');
        if ($jugadores->num_rows() > 0) {
            return $jugadores->result();
        } else {
            return false;
        }
    }

    function obtenerPorId($id) {
        $this->db->where('id_jug', $id);
        $jugador = $this->db->get('jugador');
        if ($jugador->num_rows() > 0) {
            return $jugador->row();
        } else {
            return false;
        }
    }

    function eliminar($id) {
        $this->db->where('id_jug', $id);
        return $this->db->delete('jugador');
    }

    function actualizar($id, $datos) {
        $this->db->where('id_jug', $id);
        return $this->db->update('jugador', $datos);
    }
}
