<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipo extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function insertar($datos)
    {
        return $this->db->insert('equipo', $datos);
    }

    public function obtenerTodos()
    {
        $equipos = $this->db->get('equipo');
        if ($equipos->num_rows() > 0) {
            return $equipos->result();
        } else {
            return false;
        }
    }

    public function obtenerPorId($id)
    {
        $this->db->where('id_equi', $id);
        $equipo = $this->db->get('equipo');
        if ($equipo->num_rows() > 0) {
            return $equipo->row();
        } else {
            return false;
        }
    }

    public function obtenerNombrePorId($id)
    {
        $this->db->select('nombre_equi');
        $this->db->where('id_equi', $id);
        $query = $this->db->get('equipo');

        if ($query->num_rows() > 0) {
            return $query->row()->nombre_equi;
        }

        return null;
    }

    public function eliminar($id)
    {
        try {
            $this->db->where('id_equi', $id);
            return $this->db->delete('equipo');
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function actualizar($id, $datos)
    {
        $this->db->where('id_equi', $id);
        return $this->db->update('equipo', $datos);
    }
}
?>
