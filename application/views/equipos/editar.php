<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Editar Equipo</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-editar-equipo" enctype="multipart/form-data"
                action="<?php echo site_url('equipos/actualizarEquipo') ?>" method="POST">
                <input type="hidden" name="id_equi" value="<?php echo $equipoEditar->id_equi; ?>">

                <div class="col-6 mt-4">
                    <label for="form-label">Nombre del Equipo</label>
                    <input placeholder="Nombre del equipo" type="text" class="form-control" required name="nombre_equi"
                        id="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Siglas del Equipo</label>
                    <input placeholder="Siglas" type="text" class="form-control" required name="siglas_equi"
                        id="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Año de Fundación</label>
                    <input placeholder="Año de fundación" type="number" class="form-control" required name="fundacion_equi"
                        id="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Región</label>
                    <input placeholder="Región" type="text" class="form-control" required name="region_equi"
                        id="region_equi" value="<?php echo $equipoEditar->region_equi; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Número de Títulos</label>
                    <input placeholder="Número de títulos" type="number" class="form-control" required name="numero_titulos_equi"
                        id="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>">
                </div>

                <div class="col-md-6 mt-4">
                    <button class="btn btn-success" type="submit">Actualizar Equipo</button>
                    &nbsp;
                    <a href="<?php echo site_url('equipos/index') ?>" class="btn">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#imagen_equi").fileinput({
            language: "es",
            maxFileSize: 5000,
            showUpload: false,
        });
    });
</script>
