<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Equipos</h2>
                </div>
            </div>
        </div>

        <div class="mb-4">
            <a href="<?php echo site_url('equipos/nuevo'); ?>" class="btn btn-custom">
                Agregar Equipo <i class="bi bi-plus-circle"></i>
            </a>
        </div>

        <div>
            <div class="">
                <?php if ($equipos): ?>
                    <table class="table responsive table-striped" id="tabla">
                        <thead>
                            <tr>
                                <th class="px-4 py-3">Nombre</th>
                                <th class="px-4 py-3">Siglas</th>
                                <th class="px-4 py-3">Año de Fundación</th>
                                <th class="px-4 py-3">Región</th>
                                <th class="px-4 py-3">Número de Títulos</th>
                                <th class="px-4 py-3">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($equipos as $equipo): ?>
                                <tr class="text-gray-700 dark:text-gray-400">
                                    <td class="px-4 py-3">
                                        <?php echo $equipo->nombre_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $equipo->siglas_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $equipo->fundacion_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $equipo->region_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $equipo->numero_titulos_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <a href="<?php echo site_url('equipos/editar/') . $equipo->id_equi; ?>">Editar</a>
                                        <a href="javascript:void(0)" onclick="confirmarEliminar('<?php echo site_url('equipos/eliminar/') . $equipo->id_equi; ?>', 'Equipo');">Eliminar</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p>No hay equipos registrados</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    function confirmarEliminar(url, nombre) {
        if (confirm('¿Estás seguro de que deseas eliminar ' + nombre + '?')) {
            window.location.href = url;
        }
    }
</script>
