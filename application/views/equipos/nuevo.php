<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Agregar Equipo</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-agregar-equipo" action="<?php echo site_url('equipos/guardarEquipo') ?>" method="POST">
                <div class="col-6 mt-4">
                    <label for="nombre_equi">Nombre</label>
                    <input type="text" class="form-control" id="nombre_equi" name="nombre_equi" required>
                </div>
                <div class="col-6 mt-4">
                    <label for="siglas_equi">Siglas</label>
                    <input type="text" class="form-control" id="siglas_equi" name="siglas_equi" required>
                </div>
                <div class="col-6 mt-4">
                    <label for="fundacion_equi">Año de Fundación</label>
                    <input type="number" class="form-control" id="fundacion_equi" name="fundacion_equi" required>
                </div>
                <div class="col-6 mt-4">
                    <label for="region_equi">Región</label>
                    <input type="text" class="form-control" id="region_equi" name="region_equi" required>
                </div>
                <div class="col-6 mt-4">
                    <label for="numero_titulos_equi">Número de Títulos</label>
                    <input type="number" class="form-control" id="numero_titulos_equi" name="numero_titulos_equi" required>
                </div>

                <div class="col-md-6 mt-4">
                    <button class="btn btn-success" type="submit">Guardar Equipo</button>
                    <a href="<?php echo site_url('equipos/index') ?>" class="btn btn-secondary">Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</div>
