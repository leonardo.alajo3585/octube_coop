<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Editar Jugador</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-editar-jugador" action="<?php echo site_url('jugadores/actualizarJugador') ?>"
                method="POST">
                <input type="hidden" name="id_jug" value="<?php echo $jugadorEditar->id_jug; ?>">
                <div class="col-6 mt-4">
                    <label for="">Equipo</label>
                    <select required name="fk_id_equi" id="fk_id_equi" class="form-control">
                        <option value="">--Seleccione el Equipo--</option>
                        <?php foreach ($equipos as $equipo): ?>
                            <option value="<?php echo $equipo->id_equi; ?>" <?php echo ($equipo->id_equi == $jugadorEditar->fk_id_equi) ? 'selected' : ''; ?>>
                                <?php echo $equipo->nombre_equi; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-6 mt-4">
                    <label for="form-label">Nombre</label>
                    <input placeholder="Juan" type="text" class="form-control" required name="nombre_jug"
                        id="nombre_jug" value="<?php echo $jugadorEditar->nombre_jug; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Apellido</label>
                    <input placeholder="Perez" type="text" class="form-control" required name="apellido_jug"
                        id="apellido_jug" value="<?php echo $jugadorEditar->apellido_jug; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Estatura (cm)</label>
                    <input placeholder="180" type="text" class="form-control" required name="estatura_jug"
                        id="estatura_jug" value="<?php echo $jugadorEditar->estatura_jug; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Salario</label>
                    <input placeholder="1500.00" type="text" class="form-control" required name="salario_jug"
                        id="salario_jug" value="<?php echo $jugadorEditar->salario_jug; ?>">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Estado</label>
                    <input placeholder="Activo" type="text" class="form-control" required name="estado_jug"
                        id="estado_jug" value="<?php echo $jugadorEditar->estado_jug; ?>">
                </div>

                <div class="col-12 mt-4">
                    <label for="form-label">Posición</label>
                    <select required name="fk_id_pos" id="fk_id_pos" class="form-control">
                        <option value="">--Seleccione la Posición--</option>
                        <?php foreach ($posiciones as $posicion): ?>
                            <option value="<?php echo $posicion->id_pos; ?>" <?php echo ($posicion->id_pos == $jugadorEditar->fk_id_pos) ? 'selected' : ''; ?>>
                                <?php echo $posicion->nombre_pos; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-success" type="submit">Actualizar Jugador</button>
                    &nbsp;
                    <a href="<?php echo site_url('jugadores/index') ?>" class="btn">Cancelar</a>
                </div>

            </form>
        </div>

    </div>
</div>

<script>
    function initMap() {
        const coordCentral = new google.maps.LatLng(<?php echo $jugadorEditar->latitud_jug; ?>, <?php echo $jugadorEditar->longitud_jug; ?>);
        const mapa = document.getElementById('mapa');
        const miMapa = new google.maps.Map(mapa, {
            center: coordCentral,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })

        const marcador = new google.maps.Marker({
            position: coordCentral,
            map: miMapa,
            title: 'Selecciona la ubicacion del jugador',
            draggable: true,
            icon: '<?php echo base_url('assets/images/jugador.svg') ?>'
        })

        google.maps.event.addListener(
            marcador,
            'dragend',
            function () {
                const latitud = this.getPosition().lat();
                const longitud = this.getPosition().lng();

                document.getElementById('latitud_jug').value = latitud;
                document.getElementById('longitud_jug').value = longitud;
            }
        )
    }
</script>

<script>
    document.getElementById('provincia_cor').value = "<?php echo $jugadorEditar->provincia_cor; ?>";
    document.getElementById('id_coo').value = "<?php echo $jugadorEditar->id_coo; ?>";
</script>
