<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Agregar Jugador</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-agregar-jugador" action="<?php echo site_url('jugadores/guardarJugador') ?>"
                method="POST">
                <div class="col-6 mt-4">
                    <label for="">Equipo</label>
                    <select required name="fk_id_equi" id="fk_id_equi" class="form-control">
                        <option value="">--Seleccione el Equipo--</option>
                        <?php foreach ($equipos as $equipo): ?>
                            <option value="<?php echo $equipo->id_equi; ?>">
                                <?php echo $equipo->nombre_equi; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-6 mt-4">
                    <label for="form-label">Nombre</label>
                    <input placeholder="Juan" type="text" class="form-control" required name="nombre_jug"
                        id="nombre_jug">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Apellido</label>
                    <input placeholder="Perez" type="text" class="form-control" required name="apellido_jug"
                        id="apellido_jug">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Estatura</label>
                    <input placeholder="1.80" type="text" class="form-control" required name="estatura_jug"
                        id="estatura_jug">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Salario</label>
                    <input placeholder="1000.00" type="text" class="form-control" required name="salario_jug"
                        id="salario_jug">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Estado</label>
                    <input placeholder="Activo" type="text" class="form-control" required name="estado_jug"
                        id="estado_jug">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Posición</label>
                    <select required name="fk_id_pos" id="fk_id_pos" class="form-control">
                        <option value="">--Seleccione la Posición--</option>
                        <?php foreach ($posiciones as $posicion): ?>
                            <option value="<?php echo $posicion->id_pos; ?>">
                                <?php echo $posicion->nombre_pos; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="col-12 mt-4">
                    <button class="btn btn-success" type="submit">Guardar Jugador</button>
                    &nbsp;
                    <a href="<?php echo site_url('jugadores/index') ?>" class="btn">Cancelar</a>
                </div>

            </form>
        </div>

    </div>
</div>
