<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Jugadores</h2>
                </div>
            </div>
        </div>

        <div class="mb-4">
            <a href="<?php echo site_url('jugadores/nuevo'); ?>" class="btn btn-custom">
                Agregar Jugador <i class="bi bi-plus-circle"></i>
            </a>
        </div>

        <div>
            <div class="">
                <?php if ($jugadores): ?>
                    <table class="table responsive table-striped" id="tabla">
                        <thead>
                            <tr>
                                <th class="px-4 py-3">Nombre</th>
                                <th class="px-4 py-3">Apellido</th>
                                <th class="px-4 py-3">Estatura</th>
                                <th class="px-4 py-3">Salario</th>
                                <th class="px-4 py-3">Estado</th>
                                <th class="px-4 py-3">Equipo</th>
                                <th class="px-4 py-3">Posición</th>
                                <th class="px-4 py-3">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($jugadores as $jugador): ?>
                                <tr class="text-gray-700 dark:text-gray-400">
                                    <td class="px-4 py-3">
                                        <?php echo $jugador->nombre_jug ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $jugador->apellido_jug ?>
                                    </td>
                                    <td class="px-4 py-3 text-xs">
                                        <?php echo $jugador->estatura_jug ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $jugador->salario_jug ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $jugador->estado_jug ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $jugador->nombre_equi ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $jugador->nombre_pos ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <a href="<?php echo site_url('jugadores/editar/') . $jugador->id_jug; ?>">Editar</a>
                                        <a href="javascript:void(0)" onclick="confirmarEliminar('<?php echo site_url('jugadores/eliminar/') . $jugador->id_jug; ?>', 'Jugador');">Eliminar</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p>No hay jugadores registrados</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
