<!DOCTYPE html>
<html lang="en">

<head>
   <!-- basic -->
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- mobile metas -->
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1">
   <!-- site metas -->
   <title>Futbol</title>
   <meta name="keywords" content="">
   <meta name="description" content="">
   <meta name="author" content="">
   <!-- site icon -->
   <link rel="icon" type="image/x-icon" href="../assets/images/futbol.png" />

   <!-- site icon -->
   <link rel="icon" href="<?php echo base_url('assets/images/futbol.png') ?>" type="image/png" />
   <!--TODO: Sweet alert-->
   <link href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.5/dist/sweetalert2.min.css" rel="stylesheet">
   <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.5/dist/sweetalert2.all.min.js"></script>
   <!-- jQuery -->
   <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
   <!--TODO: API GOOGLE -->
   <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxuIEYr6QvIHH0hRbPF6oHcpoc2dWpdG8&libraries=places&callback=initMap"></script>
   <script src="<?php echo base_url('assets/js/sweet.js') ?>"></script>
   <!--File input-->
   <link rel="stylesheet" href="<?php echo base_url('assets/library/file-input/file-input.min.css'); ?>" />
   <script src="<?php echo base_url('assets/library/file-input/file-input.min.js'); ?>"></script>

   <!-- Jquery Validate -->
   <script src="<?php echo base_url('assets/library/jquery-validate/jquery.validate.js'); ?>"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.19.5/additional-methods.min.js"></script>

   <!--data table -->
   <link rel="stylesheet" href="<?php echo base_url('assets/library/data-table/data-table.min.css'); ?>" />

   <!--Table data settings-->
   <script src="<?php echo base_url('assets/js/table.js'); ?>"></script>

   <!-- datatable reportes -->
   <script src="<?php echo base_url('assets/library/data-table/data-table.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets/library/data-table/data-botones.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets/library/data-table/data-print.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets/library/data-table/data-pdf.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets/library/data-table/data-pdf-make.min.js'); ?>"></script>
   <script src="<?php echo base_url('assets/library/data-table/data-botones-html.min.js'); ?>"></script>

   <!-- bootstrap css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" />
   <!-- site css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>" />
   <!-- responsive css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/responsive.css') ?>" />
   <!-- color css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/colors.css') ?>" />
   <!-- select bootstrap -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap-select.css') ?>" />
   <!-- scrollbar css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/perfect-scrollbar.css') ?>" />
   <!-- custom css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>" />
   <link rel="stylesheet" href="<?php echo base_url('assets/css/animate.css') ?>" />
   <link rel="stylesheet" href="<?php echo base_url('assets/css/flaticon.css') ?>" />
   <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" />
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
      integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
      crossorigin="anonymous" referrerpolicy="no-referrer" />


</head>

<body class="dashboard dashboard_1">
   <div class="full_container">
      <div class="inner_container">
         <!-- Sidebar  -->
         <nav id="sidebar">
            <div class="sidebar_blog_1">
               <div class="sidebar-header">
                  <div class="logo_section">
                     <a href="index.html"><img class="logo_icon img-responsive"
                           src="<?php echo base_url('assets/images/logo/pelota.jpg') ?>" alt="#" /></a>
                  </div>
               </div>
               <div class="sidebar_user_info">
                  <div class="icon_setting"></div>
                  <div class="user_profle_side">
                     <div class="user_img"><img class="img-responsive"
                           src="<?php echo base_url('assets/images/layout_img/new.png') ?>  " alt="#" /></div>
                     <div class="user_info">
                        <h6>Futbol</h6>
                        <p><span class="online_animation"></span> Online</p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="sidebar_blog_2">
               <h4>General</h4>
               <ul class="list-unstyled components">
                  <li><a href="<?php echo site_url('') ?>"><i class="fa fa-clock-o orange_color"></i>
                        <span>Inicio</span></a>
                  </li>
                  <li>
                     <a href="#element" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                           class="fa fa-diamond purple_color"></i> <span>Equipos</span></a>
                     <ul class="collapse list-unstyled" id="element">
                        <li><a href="<?php echo site_url('equipos/index') ?> ">> <span>Total de Equipos</span></a></li>
                        <li><a href="<?php echo site_url('equipos/nuevo') ?>">> <span>Agregar</span></a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="#element2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                           class="fa fa-table blue2_color"></i> <span>Posiciones</span></a>
                     <ul class="collapse list-unstyled" id="element2">
                        <li><a href="<?php echo site_url('posiciones/index') ?> ">> <span>Total de Posiciones</span></a></li>
                        <li><a href="<?php echo site_url('posiciones/nuevo') ?>">> <span>Agregar</span></a></li>
                     </ul>
                  </li>

                  <li>
                     <a href="#element3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i
                           class="fa fa-object-group yellow_color"></i> <span>Jugadores</span></a>
                     <ul class="collapse list-unstyled" id="element3">
                        <li><a href="<?php echo site_url('jugadores/index') ?> ">> <span>Total
                                 Jugadores</span></a>
                        </li>
                        <li><a href="<?php echo site_url('jugadores/nuevo') ?>">> <span>Agregar</span></a></li>
                     </ul>
                  </li>
               </ul>
            </div>
         </nav>
         <div id="content">
            <!-- topbar -->
            <div class="topbar">
               <nav class="navbar navbar-expand-lg navbar-light">
                  <div class="full">
                     <button type="button" id="sidebarCollapse" class="sidebar_toggle"><i
                           class="fa fa-bars"></i></button>
                     <div class="logo_section">
                        <a href="<?php echo site_url('') ?>"><img class="img-responsive"
                              src="<?php echo base_url('uploads/cooperativas/logo_fut.jpg') ?>" alt="#" /></a>
                     </div>
                     <div class="right_topbar">
                        <div class="icon_info">
                           <ul>
                              <li><a href="#"><i class="fa fa-bell-o"></i><span class="badge">2</span></a></li>
                              <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
                              <li><a href="#"><i class="fa fa-envelope-o"></i><span class="badge">3</span></a></li>
                           </ul>
                           <ul class="user_profile_dd">
                              <li>
                                 <a class="dropdown-toggle" data-toggle="dropdown"><img
                                       class="img-responsive rounded-circle"
                                       src="<?php echo base_url('assets/images/layout_img/new.png') ?>"
                                       alt="#" /><span class="name_user">ECUAFUTBOL</span></a>
                                 <div class="dropdown-menu">
                                    <a class="dropdown-item" href="profile.html">Perfil</a>
                                    <a class="dropdown-item" href="settings.html">Configuracion</a>
                                    <a class="dropdown-item" href="help.html">Ayuda</a>
                                    <a class="dropdown-item" href="#"><span>Salir</span> <i
                                          class="fa fa-sign-out"></i></a>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </nav>
            </div>

            <?php if ($this->session->flashdata('mensaje')): ?>
               <script>
                  Swal.fire({
                     icon: "success",
                     title: "<b style='color:#000;'>Perfecto<b>",
                     text: "<?php echo $this->session->flashdata('mensaje'); ?>"
                  });
               </script>
               <?php $this->session->set_flashdata('mensaje'); ?>
            <?php endif ?>
