<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Reporte General</h2>
                </div>
            </div>
        </div>

        <div id="reporteMapa" style="width:100%;height:900px;border:0;"></div>
    </div>
</div>
<script>
    function initMap() {
        var coordenadaCentral = new google.maps.LatLng(-0.2805707076097901, -78.53181468129077);
        var miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
            center: coordenadaCentral,
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        <?php if (is_array($cooperativas) && count($cooperativas) > 0): ?>
            <?php foreach ($cooperativas as $cooperativa): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $cooperativa->latitud_coo; ?>,
                    <?php echo $cooperativa->longitud_coo; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Matriz: <?php echo $cooperativa->nombre_coo; ?>",
                    icon: '<?php echo base_url('assets/images/coop.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($agencias) && count($agencias) > 0): ?>
            <?php foreach ($agencias as $agencia): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $agencia->latitud_age; ?>,
                    <?php echo $agencia->longitud_age; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Agencia: <?php echo $agencia->direccion_age; ?>",
                    icon: '<?php echo base_url('assets/images/agencia.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($cajeros) && count($cajeros) > 0): ?>
            <?php foreach ($cajeros as $cajero): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $cajero->latitud_caj; ?>,
                    <?php echo $cajero->longitud_caj; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Cajero Automatico " + " <?php echo $cajero->direccion_caj; ?>",
                    icon: '<?php echo base_url('assets/images/cajero.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
        <?php if (is_array($corresponsales) && count($corresponsales) > 0): ?>
            <?php foreach ($corresponsales as $corresponsal): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $corresponsal->latitud_cor; ?>,
                    <?php echo $corresponsal->longitud_cor; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: "Corresponsal Bancario: <?php echo $corresponsal->tipo_negocio_cor; ?>",
                    icon: '<?php echo base_url('assets/images/correspo.svg') ?>'
                });
            <?php endforeach; ?>
        <?php endif; ?>
    }
</script>