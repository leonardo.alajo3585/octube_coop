<div class="midde_cont">
   <div class="container-fluid">
      <div class="row column_title">
         <div class="col-md-12">
            <div class="page_title">
               <h2>Informacion Cooperativa</h2>
            </div>
            <div class="mb-4">
               <a href="<?php echo site_url('cooperativas/editar'); ?>" class="btn btn-custom">
                  Editar Informacion de la Cooperativa
               </a>
            </div>
         </div>
      </div>
      <div class="row column3">
         <!-- testimonial -->
         <div class="col-md-6">
            <div class="dark_bg full margin_bottom_30">
               <div class="full graph_head">
                  <div class="heading1 margin_0">
                     <h2>Mision</h2>
                  </div>
               </div>
               <div class="full graph_revenue">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="content testimonial">
                           <div id="testimonial_slider" class="carousel slide" data-ride="carousel">
                              <!-- Wrapper for carousel items -->
                              <div class="carousel-inner">
                                 <div class="item carousel-item active">
                                    <p class="testimonial">
                                       <?php echo $cooperativa->mision_coo; ?>
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="dark_bg full margin_bottom_30">
               <div class="full graph_head">
                  <div class="heading1 margin_0">
                     <h2>Vision</h2>
                  </div>
               </div>
               <div class="full graph_revenue">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="content testimonial">
                           <div id="testimonial_slider" class="carousel slide" data-ride="carousel">
                              <!-- Wrapper for carousel items -->
                              <div class="carousel-inner">
                                 <div class="item carousel-item active">
                                    <p class="testimonial">
                                       <?php echo $cooperativa->vision_coo; ?>
                                    </p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

      </div>
   </div>
   <div class="row column1">
      <div class="col-md-6 col-lg-3">
         <div class="full counter_section margin_bottom_30">
            <div class="counter_no">
               <div>
                  <p class="total_no mb-2">
                     <?php echo $cooperativa->nombre_coo; ?>
                  </p>
                  <p class="head_couter">Cooperativa</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-lg-3">
         <div class="full counter_section margin_bottom_30">
            <div class="counter_no">
               <div>
                  <p class="total_no mb-2">
                     <?php echo $cooperativa->provincia_coo; ?>
                  </p>
                  <p class="head_couter">Provincia</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-lg-3">
         <div class="full counter_section margin_bottom_30">
            <div class="counter_no">
               <div>
                  <p class="total_no mb-2">
                     <?php echo $cooperativa->ciudad_coo; ?>
                  </p>
                  <p class="head_couter">Ciudad</p>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-lg-3">
         <div class="full counter_section margin_bottom_30">
            <div class="counter_no">
               <div>
                  <?php if (!empty($cooperativa->logo_coo)): ?>
                     <a target="_blank" href="<?php echo base_url('uploads/cooperativas/') . $cooperativa->logo_coo; ?>">
                        <img width="200" src="<?php echo base_url('uploads/cooperativas/') . $cooperativa->logo_coo; ?>"
                           alt="Logo cooperativa">
                     </a>
                  <?php else: ?>
                     <p>No hay logo</p>
                  <?php endif ?>
               </div>
            </div>
         </div>
      </div>

      <div class="col-md-6 col-lg-6">
         <div class="full counter_section margin_bottom_30">
            <div class="counter_no">
               <div>
                  <p class="total_no mb-2">
                     <?php echo $cooperativa->direccion_coo; ?>
                  </p>
                  <p class="head_couter">Direccion</p>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="mapa" style="width:100%;height:900px;border:0;"></div>

<script>
   function initMap() {
      const coordCentral = new google.maps.LatLng(<?php echo $cooperativa->latitud_coo; ?>, <?php echo $cooperativa->longitud_coo; ?>);
      const mapa = document.getElementById('mapa');
      const miMapa = new google.maps.Map(mapa, {
         center: coordCentral,
         zoom: 19,
         mapTypeId: google.maps.MapTypeId.ROADMAP
      })

      const marcador = new google.maps.Marker({
         position: coordCentral,
         map: miMapa,
         title: 'Matriz: <?php echo $cooperativa->nombre_coo; ?> - <?php echo $cooperativa->direccion_coo; ?>',
         icon: '<?php echo base_url('assets/images/coop.svg') ?>'
      })
   }

</script>