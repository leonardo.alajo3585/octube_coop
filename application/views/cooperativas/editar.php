<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Editar Informacion Cooperativa</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-editar-cooperativa" enctype="multipart/form-data"
                action="<?php echo site_url('cooperativas/actualizarCooperativa') ?>" method="POST">
                <input type="text" id="id_coo" name="id_coo" value="<?php echo $cooperativa->id_coo ?>" hidden>
                <div class="col-6 mt-4">
                    <label for="form-label">Nombre</label>
                    <input type="text" class="form-control" required name="nombre_coo" id="nombre_coo"
                        value="<?php echo $cooperativa->nombre_coo; ?>">
                </div>
                <div class="col-6 mt-4">
                    <label for="form-label">Ciudad</label>
                    <input type="text" class="form-control" required name="ciudad_coo" name="ciudad_coo"
                        value="<?php echo $cooperativa->ciudad_coo; ?>">
                </div>
                <div class="col-12 mt-4">
                    <label for="form-label">Direccion</label>
                    <input type="text" class="form-control" required name="direccion_coo" id="direccion_coo"
                        value="<?php echo $cooperativa->direccion_coo; ?>">
                </div>
                <div class="col-6 mt-4">
                    <label for="form-label">Provincia</label>
                    <input type="text" class="form-control" required name="provincia_coo" name="provincia_coo"
                        value="<?php echo $cooperativa->provincia_coo; ?>">
                </div>
                <div class="col-12 mt-4">
                    <label for="form-label">Mision</label>
                    <textarea class="form-control" required name="mision_coo" name="mision_coo" rows="3">
                        <?php echo $cooperativa->mision_coo; ?></textarea>
                </div>
                <div class="col-12 mt-4">
                    <label class="form-label">Vision</label>
                    <textarea class="form-control" required name="vision_coo" name="vision_coo" rows="3">
                        <?php echo $cooperativa->vision_coo; ?></textarea>
                </div>

                <div class="col-6 mt-4">
                    <label class="form-label">Logo Actual</label>
                    <?php if (!empty($cooperativa->logo_coo)): ?>

                        <a target="_blank" href="<?php echo base_url('uploads/cooperativas/') . $cooperativa->logo_coo; ?>">
                            <img width="300" src="<?php echo base_url('uploads/cooperativas/') . $cooperativa->logo_coo; ?>"
                                alt="">
                        </a>
                    <?php else: ?>
                        <p>No hay logo</p>
                    <?php endif ?>
                </div>
                <div class="col-6 mt-4">
                    <label class="form-label">Cambiar Logo</label>
                    <input type="file" accept=".png" name="logo_coo" id="logo_coo" class="form-control">
                </div>

                <div class="col-6 mt-4">
                    <label for="" class="form-label">Latitud</label>
                    <input type="text" name="latitud_coo" id="latitud_coo" required class="form-control"
                        value="<?php echo $cooperativa->latitud_coo; ?>" readonly>
                </div>
                <div class="col-6 mt-4">
                    <label for="" class="form-label">Longitud</label>
                    <input type="text" name="longitud_coo" id="longitud_coo" required class="form-control"
                        value="<?php echo $cooperativa->longitud_coo; ?>" readonly>
                </div>
                <div id='mapa' style="margin-top:2rem;height: 400px; width: 100%;margin-bottom:2rem;"></div>

                <div class="col-md-6">
                    <button class="btn btn-success" type="submit">Actualizar Informacion</button>
                    <a href="<?php echo site_url('cooperativas/index') ?>" class="">Cancelar</a>
                </div>

            </form>
        </div>

    </div>
</div>

<script>
    function initMap() {
        const coordCentral = new google.maps.LatLng(<?php echo $cooperativa->latitud_coo; ?>, <?php echo $cooperativa->longitud_coo; ?>);
        const mapa = document.getElementById('mapa');
        const miMapa = new google.maps.Map(mapa, {
            center: coordCentral,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        })

        const marcador = new google.maps.Marker({
            position: coordCentral,
            map: miMapa,
            title: 'Selecciona la ubicacion de la Cooperativa',
            draggable: true,
            icon: '<?php echo base_url('assets/images/coop.svg') ?>'
        })

        google.maps.event.addListener(
            marcador,
            'dragend',
            function () {
                const latitud = this.getPosition().lat();
                const longitud = this.getPosition().lng();

                document.getElementById('latitud_coo').value = latitud;
                document.getElementById('longitud_coo').value = longitud;
            }
        )
    }
</script>


<script>

    $(document).ready(function () {
        $("#logo_coo").fileinput({
            lenguage: "es",
            maxFileSize: 5000,
            showUpload: false,
        });
    });
</script>