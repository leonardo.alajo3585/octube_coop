<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Agregar Posición</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-agregar-posicion" action="<?php echo site_url('posiciones/guardarPosicion') ?>"
                method="POST">
                <div class="col-6 mt-4">
                    <label for="form-label">Nombre</label>
                    <input placeholder="Nombre de la posición" type="text" class="form-control" required name="nombre_pos" id="nombre_pos">
                </div>

                <div class="col-6 mt-4">
                    <label for="form-label">Descripción</label>
                    <textarea placeholder="Descripción de la posición" class="form-control" required name="descripcion_pos" id="descripcion_pos"></textarea>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-success" type="submit">Guardar Posición</button>
                    &nbsp;
                    <a href="<?php echo site_url('posiciones/index') ?>" class="btn">Cancelar</a>
                </div>

            </form>
        </div>

    </div>
</div>
