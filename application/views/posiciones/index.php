<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Posiciones</h2>
                </div>
            </div>
        </div>

        <div class="mb-4">
            <a href="<?php echo site_url('posiciones/nuevo'); ?>" class="btn btn-custom">
                Agregar Posición <i class="bi bi-plus-circle"></i>
            </a>
        </div>

        <div>
            <div class="">
                <?php if ($posiciones): ?>
                    <table class="table responsive table-striped" id="tabla">
                        <thead>
                            <tr>
                                <th class="px-4 py-3">Nombre</th>
                                <th class="px-4 py-3">Descripción</th>
                                <th class="px-4 py-3">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($posiciones as $posicion): ?>
                                <tr class="text-gray-700 dark:text-gray-400">
                                    <td class="px-4 py-3">
                                        <?php echo $posicion->nombre_pos ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <?php echo $posicion->descripcion_pos ?>
                                    </td>
                                    <td class="px-4 py-3 text-sm">
                                        <a href="<?php echo site_url('posiciones/editar/') . $posicion->id_pos; ?>">Editar</a>
                                        <a href="javascript:void(0)"
                                            onclick="confirmarEliminar('<?php echo site_url('posiciones/eliminar/') . $posicion->id_pos; ?>', 'Posición');">Eliminar</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <p>No hay posiciones registradas</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
