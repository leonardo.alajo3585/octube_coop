<div class="midde_cont">
    <div class="container-fluid">
        <div class="row column_title">
            <div class="col-md-12">
                <div class="page_title">
                    <h2>Editar Posición</h2>
                </div>
            </div>
        </div>

        <div class="mx-auto" style="width:60%;">
            <form class="row" id="form-editar-posicion" action="<?php echo site_url('posiciones/actualizarPosicion') ?>" method="POST">
                <input type="hidden" name="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">
                <div class="col-6 mt-4">
                    <label for="">Nombre</label>
                    <input placeholder="Nombre de la posición" type="text" class="form-control" required name="nombre_pos" id="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>">
                </div>

                <div class="col-12 mt-4">
                    <label for="form-label">Descripción</label>
                    <textarea class="form-control" name="descripcion_pos" id="descripcion_pos" rows="4"><?php echo $posicionEditar->descripcion_pos; ?></textarea>
                </div>

                <div class="col-md-6 mt-4">
                    <button class="btn btn-success" type="submit">Actualizar Posición</button>
                    &nbsp;
                    <a href="<?php echo site_url('posiciones/index') ?>" class="btn btn-secondary">Cancelar</a>
                </div>
            </form>
        </div>

    </div>
</div>
