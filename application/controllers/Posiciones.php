<?php

class Posiciones extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Posicion");
    }

    public function index()
    {
        $data["posiciones"] = $this->Posicion->obtenerTodos();

        $this->load->view("header");
        $this->load->view('posiciones/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view('posiciones/nuevo');
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["posicionEditar"] = $this->Posicion->obtenerPorId($id);

        $this->load->view("header");
        $this->load->view('posiciones/editar', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        try {
            $this->Posicion->eliminar($id);
            $this->session->set_flashdata('mensaje', 'La posición fue eliminada correctamente');
            redirect('posiciones/index');
        } catch (\Throwable $th) {
            $this->session->set_flashdata('error', 'No se puede eliminar la posición');
            redirect('posiciones/index');
        }
    }

    public function guardarPosicion()
    {
        // Validación de formularios
        $this->form_validation->set_rules('nombre_pos', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion_pos', 'Descripción', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            // Si hay errores de validación, volver a cargar el formulario nuevo con los errores
            $this->load->view('header');
            $this->load->view('posiciones/nuevo');
            $this->load->view('footer');
        }
        else
        {
            // Datos de la posición para insertar
            $datosNuevaPosicion = array(
                "nombre_pos" => $this->input->post("nombre_pos"),
                "descripcion_pos" => $this->input->post("descripcion_pos"),
            );

            $this->Posicion->insertar($datosNuevaPosicion);
            $this->session->set_flashdata('mensaje', 'La posición fue registrada correctamente');
            redirect("posiciones/index");
        }
    }

    public function actualizarPosicion()
    {
        // Validación de formularios
        $this->form_validation->set_rules('id_pos', 'ID de Posición', 'required|integer');
        $this->form_validation->set_rules('nombre_pos', 'Nombre', 'required');
        $this->form_validation->set_rules('descripcion_pos', 'Descripción', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            // Si hay errores de validación, volver a cargar el formulario editar con los errores
            $id_pos = $this->input->post("id_pos");
            $data["posicionEditar"] = $this->Posicion->obtenerPorId($id_pos);

            $this->load->view("header");
            $this->load->view('posiciones/editar', $data);
            $this->load->view('footer');
        }
        else
        {
            // Datos de la posición para actualizar
            $id_pos = $this->input->post("id_pos");
            $datosPosicion = array(
                "nombre_pos" => $this->input->post("nombre_pos"),
                "descripcion_pos" => $this->input->post("descripcion_pos"),
            );

            $this->Posicion->actualizar($id_pos, $datosPosicion);
            $this->session->set_flashdata('mensaje', 'La posición fue actualizada correctamente');
            redirect("posiciones/index");
        }
    }
}
?>
