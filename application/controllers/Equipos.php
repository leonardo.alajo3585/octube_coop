<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Equipo"); // Cargamos el modelo Equipo
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["equipos"] = $this->Equipo->obtenerTodos();

        $this->load->view("header");
        $this->load->view('equipos/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view('equipos/nuevo');
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["equipoEditar"] = $this->Equipo->obtenerPorId($id);

        $this->load->view("header");
        $this->load->view('equipos/editar', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        try {
            $this->Equipo->eliminar($id);
            $this->session->set_flashdata('mensaje', 'El equipo fue eliminado correctamente');
            redirect('equipos/index');
        } catch (\Throwable $th) {
            $this->session->set_flashdata('error', 'No se puede eliminar el equipo');
            redirect('equipos/index');
        }
    }

    public function guardarEquipo()
    {
        // Validación de formularios
        $this->form_validation->set_rules('nombre_equi', 'Nombre', 'required');
        $this->form_validation->set_rules('siglas_equi', 'Siglas', 'required');
        $this->form_validation->set_rules('fundacion_equi', 'Año de Fundación', 'required|integer');
        $this->form_validation->set_rules('region_equi', 'Región', 'required');
        $this->form_validation->set_rules('numero_titulos_equi', 'Número de Títulos', 'required|integer');

        if ($this->form_validation->run() === FALSE)
        {
            // Si hay errores de validación, volver a cargar el formulario nuevo con los errores
            $this->load->view('header');
            $this->load->view('equipos/nuevo');
            $this->load->view('footer');
        }
        else
        {
            // Datos del equipo para insertar
            $datosNuevoEquipo = array(
                "nombre_equi" => $this->input->post("nombre_equi"),
                "siglas_equi" => $this->input->post("siglas_equi"),
                "fundacion_equi" => $this->input->post("fundacion_equi"),
                "region_equi" => $this->input->post("region_equi"),
                "numero_titulos_equi" => $this->input->post("numero_titulos_equi"),
            );

            $this->Equipo->insertar($datosNuevoEquipo);
            $this->session->set_flashdata('mensaje', 'El equipo fue registrado correctamente');
            redirect("equipos/index");
        }
    }

    public function actualizarEquipo()
    {
        // Validación de formularios
        $this->form_validation->set_rules('id_equi', 'ID de Equipo', 'required|integer');
        $this->form_validation->set_rules('nombre_equi', 'Nombre', 'required');
        $this->form_validation->set_rules('siglas_equi', 'Siglas', 'required');
        $this->form_validation->set_rules('fundacion_equi', 'Año de Fundación', 'required|integer');
        $this->form_validation->set_rules('region_equi', 'Región', 'required');
        $this->form_validation->set_rules('numero_titulos_equi', 'Número de Títulos', 'required|integer');

        if ($this->form_validation->run() === FALSE)
        {
            // Si hay errores de validación, volver a cargar el formulario editar con los errores
            $id_equi = $this->input->post("id_equi");
            $data["equipoEditar"] = $this->Equipo->obtenerPorId($id_equi);

            $this->load->view("header");
            $this->load->view('equipos/editar', $data);
            $this->load->view('footer');
        }
        else
        {
            // Datos del equipo para actualizar
            $id_equi = $this->input->post("id_equi");
            $datosEquipo = array(
                "nombre_equi" => $this->input->post("nombre_equi"),
                "siglas_equi" => $this->input->post("siglas_equi"),
                "fundacion_equi" => $this->input->post("fundacion_equi"),
                "region_equi" => $this->input->post("region_equi"),
                "numero_titulos_equi" => $this->input->post("numero_titulos_equi"),
            );

            $this->Equipo->actualizar($id_equi, $datosEquipo);
            $this->session->set_flashdata('mensaje', 'El equipo fue actualizado correctamente');
            redirect("equipos/index");
        }
    }
}
?>
