<?php

class Cooperativas extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->model("Cajero");
        $this->load->model("Corresponsal");
        $this->load->model("Cooperativa");
    }

    public function index()
    {
        $data["cooperativa"] = $this->Cooperativa->obtenerPorId(1);


        $this->load->view("header");
        $this->load->view('cooperativas/index', $data);
        $this->load->view('footer');
    }

    public function mapa()
    {
        $data["agencias"] = $this->Agencia->obtenerTodos();
        $data["cajeros"] = $this->Cajero->obtenerTodos();
        $data["corresponsales"] = $this->Corresponsal->obtenerTodos();
        $data["cooperativas"] = $this->Cooperativa->obtenerTodos();

        $this->load->view("header");
        $this->load->view('cooperativas/mapa', $data);
        $this->load->view('footer');
    }

    public function editar()
    {
        $data["cooperativa"] = $this->Cooperativa->obtenerPorId(1);

        $this->load->view("header");
        $this->load->view('cooperativas/editar', $data);
        $this->load->view('footer');
    }

    public function actualizarCooperativa()
    {
        $id_coo = $this->input->post("id_coo");

        $cooperativaEditar = $this->Cooperativa->obtenerPorId($id_coo);

        // Configuración de carga de archivos
        $config['upload_path'] = APPPATH . '../uploads/cooperativas/';
        $config['allowed_types'] = 'png';
        $config['max_size'] = 5000; // 5MB

        if (!empty($_FILES['logo_coo']['name'])) {
            $this->load->library('upload', $config);

            $nombre_archivo = $_FILES['logo_coo']['name'];

            if (file_exists($config['upload_path'] . $nombre_archivo)) {
                unlink($config['upload_path'] . $nombre_archivo);
            }

            // Subir el nuevo archivo
            if ($this->upload->do_upload('logo_coo')) {
                $dataArchivoSubido = $this->upload->data();
                $nombre_archivo_subido = $dataArchivoSubido['file_name'];
            } else {
                // Manejar errores de carga de archivo según sea necesario
                $error = array('error' => $this->upload->display_errors());
                print_r($error);
                return;
            }
        } else {
            // Si no se está cargando un nuevo archivo, mantener el nombre existente
            $nombre_archivo_subido = $cooperativaEditar->logo_coo;
        }

        $datosCooperativa = array(
            "nombre_coo" => $this->input->post("nombre_coo"),
            "ciudad_coo" => $this->input->post("ciudad_coo"),
            "provincia_coo" => $this->input->post("provincia_coo"),
            "mision_coo" => $this->input->post("mision_coo"),
            "vision_coo" => $this->input->post("vision_coo"),
            "direccion_coo" => $this->input->post("direccion_coo"),
            "latitud_coo" => $this->input->post("latitud_coo"),
            "longitud_coo" => $this->input->post("longitud_coo"),
            "logo_coo" => $nombre_archivo_subido
        );

        $this->Cooperativa->actualizar($id_coo, $datosCooperativa);
        $this->session->set_flashdata('mensaje', 'Los datos de la Cooperativa fue actualizado correctamente');
        redirect("cooperativas/index");
    }

}
