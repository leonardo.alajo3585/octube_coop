<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jugadores extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");
        $this->load->model("Equipo");
        $this->load->model("Posicion");
    }

    public function index()
    {
        $data["jugadores"] = $this->Jugador->obtenerTodos();

        // Obtener nombres de equipos y posiciones para cada jugador
        foreach ($data["jugadores"] as $jugador) {
            $jugador->nombre_equi = $this->Equipo->obtenerNombrePorId($jugador->fk_id_equi);
            $jugador->nombre_pos = $this->Posicion->obtenerNombrePorId($jugador->fk_id_pos);
        }

        $data["equipos"] = $this->Equipo->obtenerTodos();
        $data["posiciones"] = $this->Posicion->obtenerTodos();

        $this->load->view("header");
        $this->load->view('jugadores/index', $data);
        $this->load->view('footer');
    }

    public function nuevo()
    {
        $data["equipos"] = $this->Equipo->obtenerTodos();
        $data["posiciones"] = $this->Posicion->obtenerTodos();

        $this->load->view("header");
        $this->load->view('jugadores/nuevo', $data);
        $this->load->view('footer');
    }

    public function editar($id)
    {
        $data["jugadorEditar"] = $this->Jugador->obtenerPorId($id);
        $data["equipos"] = $this->Equipo->obtenerTodos();
        $data["posiciones"] = $this->Posicion->obtenerTodos();

        $this->load->view("header");
        $this->load->view('jugadores/editar', $data);
        $this->load->view('footer');
    }

    public function eliminar($id)
    {
        $this->Jugador->eliminar($id);
        $this->session->set_flashdata('mensaje', 'El jugador fue eliminado correctamente');
        redirect('jugadores/index');
    }

    public function guardarJugador()
    {
        $datosNuevoJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi"),
        );

        $this->Jugador->insertar($datosNuevoJugador);
        $this->session->set_flashdata('mensaje', 'El jugador fue registrado correctamente');
        redirect("jugadores/index");
    }

    public function actualizarJugador()
    {
        $id_jug = $this->input->post("id_jug");

        $datosJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi"),
        );

        $this->Jugador->actualizar($id_jug, $datosJugador);
        $this->session->set_flashdata('mensaje', 'El jugador fue actualizado correctamente');
        redirect("jugadores/index");
    }
}
?>
